var chromecastapp = angular.module('chromecastapp', []);

chromecastapp.controller('home', function ($scope) {
    $scope.directory = "/";
    $scope.directories = [];
    $scope.files = [];
    $scope.session = null;
    $scope.baseUrl = null;

    $scope.setDirectory = function(path) {
        getDirectories(path, function(data){
            $scope.directory = path;
            $scope.directories = data;
            $scope.$apply();
        }, function() {
        });
        getFiles(path, function(data){
            $scope.files = data;
            $scope.$apply();
        }, function(){});
    };

    $scope.openFile = function(file) {
    if(!file.mimeType)
    file.mimeType = "video/mp4";
    console.log("Trying to stream:"+ file.name +", "+ file.mimeType);
        var mediaInfo = new chrome.cast.media.MediaInfo($scope.baseUrl +"/api/browsing/file?path="+ encodeURIComponent(file.path), file.mimeType);
        mediaInfo.streamType = chrome.cast.media.StreamType.BUFFERED;
        var request = new chrome.cast.media.LoadRequest(mediaInfo);
        $scope.session.loadMedia(request,
           function(med, type){console.log(med);}.bind(this, 'loadMedia'),
           function(e){console.log(e);console.log("fat error here.")});
    };

    $scope.startSession = function() {
        ChromeCast.createSession(function(e){
            $scope.session = e;
        }, function(){})
    };


    getHomeDirectory(function(data){
        $scope.setDirectory(data.path);
    }, function(){});

    getBaseUrl(function(data){
        $scope.baseUrl = data;
    },function(e){
        console.log(e);
    });
});
function getBaseUrl(success, failed) {
$.ajax({    method:     "get",
        url:            "/api/network/baseurl",
        success:        success,
        error : failed
   });
}
function getHomeDirectory(success, failed) {
$.ajax({    method:     "get",
        contentType:    "application/json; charset=utf-8",
        dataType:       "json",
        url:            "/api/browsing/home",
        success:        success,
        error : failed
   });
}

function getDirectories(path, success, failed){
        $.ajax({    method:         "post",
                    contentType:    "application/json; charset=utf-8",
                    dataType:       "json",
                    url:            "/api/browsing/directories",
                    data:           JSON.stringify({ path : path }),
                    success:        success,
                    error : failed
               });
}

function getFiles(path, success, failed) {
        $.ajax({    method:         "post",
                    contentType:    "application/json; charset=utf-8",
                    dataType:       "json",
                    url:            "/api/browsing/files",
                    data:           JSON.stringify({ path : path }),
                    success:        success,
                    error : failed
               });
}