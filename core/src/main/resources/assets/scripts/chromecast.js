function ChromeCast() {}

ChromeCast.init = function() {
    var sessionRequest = new chrome.cast.SessionRequest(chrome.cast.media.DEFAULT_MEDIA_RECEIVER_APP_ID);
    var apiConfig = new chrome.cast.ApiConfig(sessionRequest, function(e){/*session listener */
        console.log("session listener.");

    }, function(e){/*receiver listener*/


    console.log("receiver listener!");
    });
    chrome.cast.initialize(apiConfig, function(e) {
    /*success*/
    console.log("SUCCES!");
    }, function() {
    console.log("ERROR!");
    /*error*/
    });
};

ChromeCast.creatingSession = false;
ChromeCast.createSession = function(success, error) {
    if(!ChromeCast.creatingSession) {
        ChromeCast.creatingSession = true;
        chrome.cast.requestSession(function(e){
            ChromeCast.creatingSession = false;
            if(success)
                success(e);
        }, function(){
            ChromeCast.creatingSession = false;
            if(error)
                error();
        });
    }
};

window['__onGCastApiAvailable'] = function(loaded, errorInfo) {
  if (loaded) {
    ChromeCast.init();
  } else {
    console.log(errorInfo);
  }
};