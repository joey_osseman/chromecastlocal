package osseman.chromecast.rest;

import java.io.File;

/**
 * Created by joeyosseman on 28/05/14.
 */
public class Directory {
    private String name;
    private String path;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    public static Directory fromFile(File file) {
        return fromFile(file, file.getName());
    }

    public static Directory fromFile(File file, String name) {
        Directory directory = new Directory();
        directory.setName(name);
        directory.setPath(file.getAbsolutePath());
        return directory;
    }
}
