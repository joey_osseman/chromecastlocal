package osseman.chromecast.rest;

import javax.ws.rs.*;
import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.StreamingOutput;
import java.io.*;
import java.nio.channels.Channels;
import java.nio.channels.FileChannel;
import java.nio.channels.WritableByteChannel;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

/**
 * Created by joeyosseman on 28/05/14.
 */
@Path("/browsing")
public class BrowsingResource {

    @Path("directories")
    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public List<Directory> getDirectories(Directory dir) {
        if(dir == null)
            throw new IllegalArgumentException("Directory is required.");

        return getDirectoriesByDirectory(dir);
    }

    @Path("home")
    @GET
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Directory getHomeDirectory() {
        return Directory.fromFile(new File(System.getProperty("user.home")));
    }

    @Path("roots")
    @GET
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public List<Directory> getRoots() {
        File[] roots = File.listRoots();
        ArrayList<Directory> directoryRoots = new ArrayList<>(roots.length);
        for(File root : roots) {
            directoryRoots.add(Directory.fromFile(root));
        }

        return directoryRoots;
    }

    @Path("files")
    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public List<FileInfo> getFiles(Directory dir) {
        if(dir == null)
            throw new IllegalArgumentException("Directory is required.");

        ArrayList<FileInfo> files = new ArrayList<>();
        File dirFile = new File(dir.getPath());
        for(File file : dirFile.listFiles(x -> x.isFile() && !x.isHidden() && x.canRead() && x.getName().contains("."))) {
            files.add(FileInfo.fromFile(file));
        }

        return files;
    }


    @Path("file")
    @GET
    @Produces("video/mp4")
    public Response getFile(@QueryParam("path") String path, @HeaderParam("Range") String range) throws Exception {
        return buildStream(new File(path), range);
    }

    private Response buildStream(final File asset, final String range) throws Exception {
        // range not requested : Firefox, Opera, IE do not send range headers
        if (range == null) {
            StreamingOutput streamer = new StreamingOutput() {
                @Override
                public void write(final OutputStream output) throws IOException, WebApplicationException {

                    final FileChannel inputChannel = new FileInputStream(asset).getChannel();
                    Channels Channels;
                    final WritableByteChannel outputChannel = java.nio.channels.Channels.newChannel(output);
                    try {
                        inputChannel.transferTo(0, inputChannel.size(), outputChannel);
                    } finally {
                        // closing the channels
                        inputChannel.close();
                        outputChannel.close();
                    }
                }
            };
            return Response.ok(streamer).status(200).header(HttpHeaders.CONTENT_LENGTH, asset.length()).build();
        }

        String[] ranges = range.split("=")[1].split("-");
        final int from = Integer.parseInt(ranges[0]);
        /**
         * Chunk media if the range upper bound is unspecified. Chrome sends "bytes=0-"
         */
        int to = 1024 + from;
        if (to >= asset.length()) {
            to = (int) (asset.length() - 1);
        }
        if (ranges.length == 2) {
            to = Integer.parseInt(ranges[1]);
        }

        final String responseRange = String.format("bytes %d-%d/%d", from, to, asset.length());
        final RandomAccessFile raf = new RandomAccessFile(asset, "r");
        raf.seek(from);

        final int len = to - from + 1;
        final MediaStreamer streamer = new MediaStreamer(len, raf);
        Response.ResponseBuilder res = Response.ok(streamer).status(206)
                .header("Accept-Ranges", "bytes")
                .header("Content-Range", responseRange)
                .header(HttpHeaders.CONTENT_LENGTH, streamer.getLenth())
                .header(HttpHeaders.LAST_MODIFIED, new Date(asset.lastModified()));
        return res.build();
    }

    private List<Directory> getDirectoriesByDirectory(Directory currentDirectory) {
        ArrayList<Directory> directories = new ArrayList<>();
        File currentDirectoryFile = new File(currentDirectory.getPath());
        if(currentDirectoryFile.exists()) {
            if(currentDirectoryFile.getParentFile() != null) {
                directories.add(Directory.fromFile(currentDirectoryFile.getParentFile(), "..."));
            }

            for(File file : currentDirectoryFile.listFiles(x -> x.isDirectory() && !x.isHidden() && x.canRead())) {
                directories.add(Directory.fromFile(file));
            }
        }

        return directories;
    }
}
