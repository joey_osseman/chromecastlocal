package osseman.chromecast.rest;

import java.io.File;
import java.io.IOException;
import java.net.URLConnection;
import java.nio.file.Files;
import java.nio.file.Path;

/**
 * Created by joeyosseman on 29/05/14.
 */
public class FileInfo {
    private String name;
    private String path;
    private String mimeType;

    public void setName(String name) {
        this.name = name;
    }

    public void setPath(String path) {
        this.path = path;
    }

    public void setMimeType(String mimeType) {
        this.mimeType = mimeType;
    }

    public String getName() {
        return name;
    }

    public String getPath() {
        return path;
    }

    public String getMimeType() {
        return mimeType;
    }

    public static FileInfo fromFile(File file) {
        return fromFile(file, file.getName());
    }

    public static FileInfo fromFile(File file, String name) {
        FileInfo directory = new FileInfo();
        directory.setName(name);
        directory.setPath(file.getAbsolutePath());
        directory.setMimeType(URLConnection.guessContentTypeFromName(file.getName()));
        return directory;
    }
}
