package osseman.chromecast.rest;

import sun.net.util.IPAddressUtil;

import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.core.Context;
import java.net.Inet4Address;
import java.net.NetworkInterface;
import java.net.SocketException;
import java.net.UnknownHostException;
import java.util.*;

/**
 * Created by joeyosseman on 31/05/14.
 */
@Path("network")
public class NetworkResource {

    @GET
    @Path("baseurl")
    public String getBaseUrl(@Context HttpServletRequest req) throws UnknownHostException {
        return String.format("http://%s:%d", Inet4Address.getLocalHost().getHostAddress(), req.getLocalPort());
    }

    public List<NetworkInterface> getPublicInterfaces() throws SocketException {
        ArrayList<NetworkInterface> nets = new ArrayList<>();
        for(NetworkInterface x : Collections.list(NetworkInterface.getNetworkInterfaces())) {
            if((x.isLoopback() || x.isPointToPoint() || x.isVirtual() || !x.isUp()))
                continue;

            nets.add(x);
        }

        return nets;
   }
}
