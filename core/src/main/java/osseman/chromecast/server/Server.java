package osseman.chromecast.server;

import io.dropwizard.Application;
import io.dropwizard.assets.AssetsBundle;
import io.dropwizard.setup.Bootstrap;
import io.dropwizard.setup.Environment;
import osseman.chromecast.rest.BrowsingResource;
import osseman.chromecast.rest.NetworkResource;

public class Server extends Application<ApplicationConfiguration> {
    public static void main(String[] args) throws Exception {
        Server server = new Server();
        server.run(args);
    }

    @Override
    public String getName() {
        return "chromecast";
    }

    @Override
    public void initialize(Bootstrap<ApplicationConfiguration> bootstrap) {
        bootstrap.addBundle(new AssetsBundle("/assets/", "/", "index.html"));
    }

    @Override
    public void run(ApplicationConfiguration configuration,
                    Environment environment) {
        environment.jersey().setUrlPattern("/api/*");
        environment.jersey().register(new BrowsingResource());
        environment.jersey().register(new NetworkResource());
    }
}