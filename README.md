# README #

This project makes it possible to stream local files to the ChromeCast. The application starts a webserver on the local machine, which then can be browsed by Chrome. The rest is pretty straight forward.

## Contributing ##
Contributions are always welcome! Please contact me at joey.osseman@gmail.com.